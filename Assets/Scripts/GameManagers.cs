﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDetails
{
	public Vector3[] mMovedPositions;
	public GameObject mTouchedObject;
}
public class GameManagers
{

	private int mLevel,mMoveCount,mBestMoveCount,mNumberOfStars;          
	private static GameManagers mInstance;
	private string mPath, mMode;
	private GameObject mKingBahaviourControl;
	private bool mInitialize=true, mSound,mvibration, mPlaying=false;
	internal Dictionary<Color, bool> mConnected = new Dictionary<Color, bool>();
	internal Dictionary<string, UserStatus> mLevelStatus =ConfigManager.Instance.mUserDetails;
	internal List<GameObject> mDots = new List<GameObject>();
	internal List<GameObject> mFillIndicator = new List<GameObject>();
	internal Stack<MoveDetails> mStack = new Stack<MoveDetails>();
	internal int[,] FillArray;
	private GameManagers(){    }
    public static GameManagers Instance{
        get{
            if(mInstance==null)
            {
                mInstance=new GameManagers();
            }
            return mInstance;
        }
    }
	public bool Initialize
	{
		get
		{
			return mInitialize;
		}
		set
		{
			mInitialize = value;
		}
	}
	public bool IsPlaying
	{
		get
		{
			return mPlaying;
		}
		set
		{
			mPlaying = value;
		}
	}
	public int Level
	{
		get
		{
			return mLevel;
		}
		set
		{
			mLevel = value;
		}
	}
	public string Mode
	{
		get
		{
			return mMode;
		}
		set
		{
			mMode = value;
		}
	}
	public int BestMovBestMoveCount
	{
		get
		{
			return mBestMoveCount;
		}
		set
		{
			mBestMoveCount = value;
		}
	}
	public int NumberOfStars
	{
		get
		{
			return mNumberOfStars;
		}
		set
		{
			mNumberOfStars = value;
		}
	}
	public int MoveCount
	{
		get
		{
			return mMoveCount;
		}
		set
		{
			mMoveCount = value;
		}
	}
	public bool IsSoundOn
	{
		get
		{
			return mSound;
		}
		set
		{
			mSound = value;
		}
	}
	public bool IsVibrationOn
	{
		get
		{
			return mvibration;
		}
		set
		{
			mvibration = value;
		}
	}
	internal void LoadLevel()
	{
		ResetValues();
		InitializeNextLevel();
		mPath = "Prefabs/King/KingBehaviour";
		mKingBahaviourControl = InstantiatePrefab.InstantiateObject(mPath);
		GridGenerator.Instance.InitialiseKings(Level,mMode);
	}
	internal void LoadNextLevel()
	{
		UpdateUserData();
		ResetValues();
		ConfigManager.Instance.StoreUserDetails();
		mLevel++;
		InitializeNextLevel();
		mPath = "Prefabs/King/KingBehaviour";
		mKingBahaviourControl = InstantiatePrefab.InstantiateObject(mPath);
		GridGenerator.Instance.InitialiseKings(mLevel,mMode);
	}

	internal void UpdateUserData()
	{
		Level LevelData= new Level();
		LevelData.level = Level;
		LevelData.Unlocked = true;
		LevelData.NumberOfStars = NumberOfStars;
		//check status having Level Data Stored
		if (!mLevelStatus[Mode].mLevelStatus.ContainsKey(Level))
		{
			mLevelStatus[Mode].mLevelStatus.Add(Level, LevelData);
		}
		//already user status having key so just update value
		else
			mLevelStatus[Mode].mLevelStatus[Level] = LevelData;
	}

	private void InitializeNextLevel()
	{
		Level LevelData = new Level();
		LevelData.level = Level+1;
		LevelData.Unlocked = true;
		LevelData.NumberOfStars = 0;
		if (!mLevelStatus[Mode].mLevelStatus.ContainsKey(Level))
		{
			mLevelStatus[Mode].mLevelStatus.Add(Level, LevelData);
		}
		ConfigManager.Instance.StoreUserDetails();
	}

	void ResetValues()
	{
		ClearStack();
		mInitialize = true;
		GridGenerator.Instance.ClearGrid();
		KingBehaviourControl.Instance.gameObjectList.Clear();
		mConnected.Clear();
	}

	private void ClearStack()
	{
		mStack.Clear();
	}
}
