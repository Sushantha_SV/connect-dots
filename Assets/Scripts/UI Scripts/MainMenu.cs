﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class MainMenu:MonoBehaviour
{

	[SerializeField]
	private RectTransform Title, mEasyObject, mNormalObject, mHardObject, mMasterObject, MainMenuPanel;
	[SerializeField]
	private Button mEasy,mNormal,mHard,mMaster,mBack,mSetting, mYesButton, mNoButton;
	string mPath;
	GameObject mObject;
	internal bool playing;
	GameObject mCanvas,KingBehaviour,mParentMapper;
	
	public static MainMenu Instance;

	private void Awake()
	{
		if (Instance == null)
			Instance = this;
		else if (Instance != this)
			Destroy(gameObject);
		
		mEasy.onClick.AddListener(OnEasy);
		mNormal.onClick.AddListener(OnNormal);
		mHard.onClick.AddListener(OnHard);
		mMaster.onClick.AddListener(OnMaster);
		mBack.onClick.AddListener(OnBack);
		mSetting.onClick.AddListener(OnSetting);
		//
		mCanvas = GameObject.FindWithTag("Canvas");
		mParentMapper= GameObject.FindWithTag("Mapper");
		//Title.DOAnchorPos(new Vector2(10f,0f),0.5f);
		Title.DOAnchorPos(Vector2.zero, 0.25f).SetEase(Ease.InOutBack);

		ModeDraw();
	}

	private void ModeDraw()
	{
		Vector2 mPosition;
		mEasyObject.DOAnchorPos(new Vector2(0f,0f), 0.35f).SetEase(Ease.InOutBounce).SetDelay(1f); ;
		mNormalObject.DOAnchorPos(new Vector2(0f, 0f), 0.35f).SetEase(Ease.InOutBounce).SetDelay(1f); 
		mHardObject.DOAnchorPos(new Vector2(0f, 0f), 0.35f).SetEase(Ease.InOutBounce).SetDelay(1f); 
		mMasterObject.DOAnchorPos(new Vector2(0f, 0f), 0.35f).SetEase(Ease.InOutBounce).SetDelay(1f); 
	}

	private void Start()
	{
		
		KingBehaviour = GameObject.Find("KingBehaviour");
		if (KingBehaviour != null)
			GameObject.Destroy(KingBehaviour);
	}
	void setParent(GameObject inObject)
	{
		inObject.transform.SetParent(mCanvas.transform, false);
	}

	void InstantiateObjecT(string inPath)
	{
		mObject = InstantiatePrefab.InstantiateObject(mPath);
	}
	void SetPath(string inPath)
	{
		mPath=inPath;
	}
	private void OnSetting()
	{
		AudioManager.mInstance.play("Back");
		SetPath("Prefabs/UI Menu Items/SettingPanel");
		InstantiateObjecT(mPath);
		setParent(mObject);
	}

	private void OnBack()
	{
		AudioManager.mInstance.play("Forward");
		Destroy(gameObject);
		SetPath("Prefabs/UI Menu Items/Quit");
		InstantiateObjecT(mPath);
		setParent(mObject);
	}
	
	private void OnMaster()
	{
		
		AudioManager.mInstance.play("Forward");
		GameManagers.Instance.Mode = "Master";

		StartCoroutine(OnOut());

		MainMenuPanel.DOAnchorPos(new Vector2(0,360f), 1f).SetEase(Ease.InOutBack).SetDelay(0.25f).OnComplete(() =>
		{
			Destroy(gameObject);
			SetPath("Prefabs/UI Menu Items/ScrolPanel");
			InstantiateObjecT(mPath);
			mObject.transform.SetParent(mParentMapper.transform, false);

			//Panel Instatiation
			SetPath("Prefabs/UI Menu Items/LevelPanel");
			InstantiateObjecT(mPath);
			setParent(mObject);
		});
		//Instantiate ScrolView
		
		
		
	}

	private void OnHard()
	{


		AudioManager.mInstance.play("Forward");
		GameManagers.Instance.Mode = "Hard";
		StartCoroutine(OnOut());

		MainMenuPanel.DOAnchorPos(new Vector2(0, 360f), 1f).SetEase(Ease.InOutBack).SetDelay(0.25f).OnComplete(() =>
		{
			Destroy(gameObject);
			//Instantiate Mapper
			SetPath("Prefabs/UI Menu Items/ScrolPanel");
			InstantiateObjecT(mPath);
			mObject.transform.SetParent(mParentMapper.transform, false);


			//Panel Instatiation
			SetPath("Prefabs/UI Menu Items/LevelPanel");
			InstantiateObjecT(mPath);
			setParent(mObject);
		});
	}

	private void OnNormal()
	{
		
		AudioManager.mInstance.play("Forward");
		GameManagers.Instance.Mode = "Normal";
		StartCoroutine(OnOut());

		MainMenuPanel.DOAnchorPos(new Vector2(0, 360f), 1f).SetEase(Ease.InOutBack).SetDelay(0.25f).OnComplete(() =>
		{
			Destroy(gameObject);

			//Instantiate Mapper
			SetPath("Prefabs/UI Menu Items/ScrolPanel");
			InstantiateObjecT(mPath);
			mObject.transform.SetParent(mParentMapper.transform, false);

			//Panel Instatiation

			SetPath("Prefabs/UI Menu Items/LevelPanel");
			InstantiateObjecT(mPath);
			setParent(mObject);
		});
	}

	private void OnEasy()
	{

		AudioManager.mInstance.play("Forward");
		GameManagers.Instance.Mode = "Easy";

		StartCoroutine(OnOut());

		MainMenuPanel.DOAnchorPos(new Vector2(0, 360f), 1f).SetEase(Ease.InOutBack).SetDelay(0.25f).OnComplete(() =>
		{
			Destroy(gameObject);
			//Instantiate Mapper
			SetPath("Prefabs/UI Menu Items/ScrollView");
			InstantiateObjecT(mPath);
			mObject.transform.SetParent(mParentMapper.transform, false);

			//Panel Instatiation
			SetPath("Prefabs/UI Menu Items/LevelPanel");
			InstantiateObjecT(mPath);
			setParent(mObject);
		});
	}
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape) )
		{
			
			ConfigManager.Instance.StoreUserDetails();
			Application.Quit();
		}

	}
	IEnumerator OnOut()
	{
		mEasyObject.DOAnchorPos(new Vector2(-190f, 55f), 0.35f).SetEase(Ease.InOutBack);
		mNormalObject.DOAnchorPos(new Vector2(190f, 55f), 0.35f).SetEase(Ease.InOutBack);
		mHardObject.DOAnchorPos(new Vector2(-190f, -55f), 0.35f).SetEase(Ease.InOutBack);
		mMasterObject.DOAnchorPos(new Vector2(190f, -55f), 0.35f).SetEase(Ease.InOutBack);
		yield return new WaitForSecondsRealtime(2f);

	}
	
	private void Quit()
	{
		Application.Quit();
	}
	
}
