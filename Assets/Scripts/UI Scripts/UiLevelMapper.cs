﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using DG.Tweening;
using UnityEngine.EventSystems;

public class UiLevelMapper : MonoBehaviour
{
	private int mRow, mCol, mLevelSelected;
	private Vector2 mGridSize, mSpawnOffset = new Vector2(0f, -0.9f);
	private string mPath, mMode;
	[SerializeField]
	private GameObject mLevelText;
	[SerializeField]
	Button mBackButton;
	GameObject mObject, mCanvas, mParent;
	[SerializeField]
	RectTransform mLevelPanel, mScrolViewPanel;
	//
	internal Vector2 mGridOffset;
	internal Vector2 mCellSize;
	internal Vector2 mCellScale;
	internal Vector2 mNewCellSize;
	//

	public static UiLevelMapper Instance;
	private void Awake()
	{

		if (Instance == null)
			Instance = new UiLevelMapper();
		else if (Instance != this)
			Destroy(gameObject);
		mBackButton.onClick.AddListener(OnBack);
		mCanvas = GameObject.FindWithTag("Canvas");
		mParent = GameObject.FindGameObjectWithTag("LevelMap");
		InitialiseGridParams();
		InstantiateObject();
	}
	void OnBack()
	{
		Destroy(gameObject);
		Clear();
		GameObject ScrolView= GameObject.FindGameObjectWithTag("ScrollView");
		Destroy(ScrolView);
		string mPath = "Prefabs/UI Menu Items/MainMenu";
		GameObject mMenu = InstantiatePrefab.InstantiateObject(mPath);
		mMenu.transform.SetParent(mCanvas.transform, false);
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			OnBack();
		}
	}
	
	private void InstantiateObject()
	{
		int column = 0, row= 3;
		for (int i = 1; i <= ConfigManager.Instance.mMetaData[GameManagers.Instance.Mode].Count; i++)
		{
			bool UnLocked= CheckLevelStatus(i,GameManagers.Instance.Mode);
			
			mObject = InstantiatePrefab.InstantiateObject(mPath);
			mObject.transform.SetParent(mParent.transform, false);
			mCellSize = mObject.GetComponent<SpriteRenderer>().bounds.size;
			if (UnLocked)
			{
				mLevelText = mObject.transform.Find("LevelText").gameObject;
				mLevelText.GetComponent<TextMeshProUGUI>().text = i.ToString();
			}
			mObject.transform.position = GetPosition(row , column++);
			if (column == 4) { column = 0; row--; }
		
			mObject.transform.localScale = new Vector2(mCellScale.x-5.2f, mCellScale.y-7.7f);
		}
	}

	private bool CheckLevelStatus(int inlevel, string mode)
	{

		//check wheteher user played this and has status
		if(GameManagers.Instance.mLevelStatus[mode].mLevelStatus.ContainsKey(inlevel))
		{
			//access the LevelData info
			var mLevelData = GameManagers.Instance.mLevelStatus[mode].mLevelStatus[inlevel];
			//check level Locked or Unlocked
			if (mLevelData.Unlocked)
			{

				//checkfor stars of level
				int NumberOfstars = mLevelData.NumberOfStars;
				switch (NumberOfstars)
				{
					case 0:
						mPath = "Prefabs/Level/ZeroStars";
						break;

					case 1:
						mPath = "Prefabs/Level/OneStar";
						break;
					case 2:
						mPath = "Prefabs/Level/TwoStar";
						break;
					case 3:
						mPath = "Prefabs/Level/ThreeStar";
						break;
				}
				return true;
			}
			else
			{
				//Locked
				mPath = "Prefabs/Level/Locked";
				return false;
			}

		}
			else
			{
				//Locked
				mPath = "Prefabs/Level/Locked";
			return false;
			}
		
	}

	public void OnClick(TextMeshProUGUI Level)
	{
		
		AudioManager.mInstance.play("Forward");
		mLevelSelected = int.Parse(Level.text);
		Debug.Log(mLevelSelected);
		Load(mLevelSelected);
		/*mLevelPanel.DOAnchorPos(new Vector2(0, 360f), 1f).SetEase(Ease.InOutBack).SetDelay(0.25f).OnComplete(() =>
		{
			A
		});*/

	}
	private void Load(int Level)
	{
		Clear();
		mPath = "Prefabs/UI Menu Items/Board";
		GameObject mBoard = InstantiatePrefab.InstantiateObject(mPath);
		mBoard.transform.SetParent(mCanvas.transform, false);

		GameManagers.Instance.Level = Level;
		GameManagers.Instance.LoadLevel();
		
	}

	private void Clear()
	{
		mCanvas = GameObject.FindGameObjectWithTag("Canvas");
		mParent = GameObject.FindGameObjectWithTag("Mapper");
		foreach (Transform child in mParent.transform)
		{
			GameObject.Destroy(child.gameObject);
		}
		foreach (Transform child in mCanvas.transform)
		{
			GameObject.Destroy(child.gameObject);
		}
	}
	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireCube(mSpawnOffset, mGridSize);
	}
	
	internal Vector2 GetPosition(int inRow, int inColumn)
	{
		mCellScale.x = mNewCellSize.x / mCellSize.x;
		mCellScale.y = mNewCellSize.y / mCellSize.y;
		mCellSize = mNewCellSize;
		mGridOffset.x = -(mGridSize.x /2) + mCellSize.x/2;
		mGridOffset.y = -(mGridSize.y /2) + mCellSize.y / 2;
		Vector2 mCellPosition = new Vector2(inColumn * ((mCellSize.x)) + (mGridOffset.x) +mSpawnOffset.x+ mCanvas.transform.position.x, inRow * ((mCellSize.y)) + mGridOffset.y +mSpawnOffset.y+ mCanvas.transform.position.y);
		return mCellPosition;
	}

	void InitialiseGridParams()
	{
		mRow = 4;
		mCol =4;
		mGridSize = new Vector2(4.10f, 6.25f);
		mNewCellSize = new Vector2(mGridSize.x / (float)mRow, mGridSize.y / (float)mCol);
	}

}
