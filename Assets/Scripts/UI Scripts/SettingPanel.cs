﻿using UnityEngine.UI;
using UnityEngine;
using System;
using TMPro;
using DG.Tweening;
public class SettingPanel : MonoBehaviour
{
	[SerializeField]
	Button mBackButton, mSaveButton, mSound, mVibration;

	[SerializeField]
	TextMeshProUGUI mSoundText, mVibrationText;
	
	[SerializeField]
	bool Sound, Vibration;
	GameObject mCanvas,mBoardPanel;
	private void Awake()
	{
		mCanvas = GameObject.FindGameObjectWithTag("Canvas");
		mBackButton.onClick.AddListener(GoBack);
		mSaveButton.onClick.AddListener(OnSave);
		mSound.onClick.AddListener(OnSoundButton);
		mVibration.onClick.AddListener(OnVibration);
		Sound=GameManagers.Instance.IsSoundOn;
		Vibration= GameManagers.Instance.IsVibrationOn;
		SetSound();
	}
	
	private void SetSound()
	{
		if (GameManagers.Instance.IsSoundOn)
		{

			AudioManager.mInstance.volume = 1f;
			mSoundText.text = "ON";
		}
		else
		{
			AudioManager.mInstance.volume = 0f;
			mSoundText.text = "OFF";
		}

		if (GameManagers.Instance.IsVibrationOn)
		{
			mVibrationText.text = "ON";
		}
		else
		{
			mVibrationText.text = "OFF";
		}
	}

	private void OnVibration()
	{
		AudioManager.mInstance.IsVibrationOn = !AudioManager.mInstance.IsVibrationOn;
		if (AudioManager.mInstance.IsVibrationOn)
		{
			mVibrationText.SetText("ON");
			GameManagers.Instance.IsVibrationOn = true;
		}
		else
		{
			mVibrationText.SetText("OFF");
			GameManagers.Instance.IsVibrationOn = false;
		}
	}

	private void OnSoundButton()
	{
		AudioManager.mInstance.IsSoundOn = !AudioManager.mInstance.IsSoundOn;
		if(AudioManager.mInstance.IsSoundOn)
		{
			AudioManager.mInstance.volume = 1f;
			mSoundText.SetText("ON");
			GameManagers.Instance.IsSoundOn = true;
		}
		else
		{
			AudioManager.mInstance.volume = 0f;
			mSoundText.SetText("OFF");
			GameManagers.Instance.IsSoundOn = false;
		}
		
	}

	private void OnSave()
	{
		ConfigManager.Instance.StoreSettingPreference();
		if (!GameManagers.Instance.IsPlaying)
		{
			Destroy(gameObject);
			string mPath = "Prefabs/UI Menu Items/MainMenu";
			GameObject mMenu = InstantiatePrefab.InstantiateObject(mPath);
			mMenu.transform.SetParent(mCanvas.transform, false);
		}
		else
		{
			Destroy(gameObject);
			mBoardPanel = GameObject.FindGameObjectWithTag("MainBoard");

			mBoardPanel.transform.GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, 0.15f, true).OnComplete(()=>
			{
				GameObject.Find("Grid").transform.parent = GameObject.Find("GameSceneController").transform;
			});
		}
	}

	private void GoBack()
	{
		AudioManager.mInstance.play("Back");
		if (GameManagers.Instance.IsPlaying)
		{
			GameObject mObject;
			mObject = GameObject.Find("KingBehaviour(Clone)");
			mObject.SetActive(true);
			//GameObject.FindGameObjectWithTag("Grid").SetActive(true);
			/*GameObject.Find("King").SetActive(true);
			GameObject.FindGameObjectWithTag("King").SetActive(true);
			Destroy(gameObject);*/
			GameManagers.Instance.IsPlaying = false;
		}
		else
		{
			Destroy(gameObject);
			string mPath = "Prefabs/UI Menu Items/MainMenu";
			GameObject mMenu = InstantiatePrefab.InstantiateObject(mPath);
			mMenu.transform.SetParent(mCanvas.transform, false);
		}
	}

	// Update is called once per frame
	void Update()
    {
		if (Input.GetKeyDown(KeyCode.Escape)&& !GameManagers.Instance.IsPlaying)
		{
			AudioManager.mInstance.play("Back");
			Destroy(gameObject);
			string mPath = "Prefabs/UI Menu Items/MainMenu";
			GameObject mMenu = InstantiatePrefab.InstantiateObject(mPath);
			mMenu.transform.SetParent(mCanvas.transform, false);
		}
	}
}
