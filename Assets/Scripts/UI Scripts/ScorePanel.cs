﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class ScorePanel : MonoBehaviour
{
	[SerializeField]
	TextMeshProUGUI mCurrentLevel, mBestMove, mMoveCount;
	[SerializeField]
	RectTransform mScorePanel;
	string mPath;
	[SerializeField]
	Button mNextlevel, mhomeButton, mPlayAgain;
	GameObject mNumberOfStars;
	public static ScorePanel Instance;
	private void Awake()
	{
		if (Instance == null)
			Instance = new ScorePanel();
		else if (Instance != this)
			Destroy(gameObject);
		mCurrentLevel.text = GameManagers.Instance.Level.ToString();
		mBestMove.text = GameManagers.Instance.BestMovBestMoveCount.ToString();
		mMoveCount.text = GameManagers.Instance.MoveCount.ToString();
		CheckStars();
		InstantiateStars();
		mNextlevel.onClick.AddListener(LoadnextLevel);
		mhomeButton.onClick.AddListener(LoadHome);
		mPlayAgain.onClick.AddListener(ReloadGame);
	}

	private void ReloadGame()
	{

		AudioManager.mInstance.play("Refresh");
		GameManagers.Instance.UpdateUserData();
		Destroy(gameObject);
		GameObject mKingBehaviour = GameObject.FindGameObjectWithTag("King");
		GameObject.Destroy(mKingBehaviour);
		GameManagers.Instance.LoadLevel();
	}

	private void InstantiateStars()
	{
		mNumberOfStars = InstantiatePrefab.InstantiateObject(mPath);
		GameObject Parent = GameObject.FindGameObjectWithTag("ScorePanel");
		mNumberOfStars.transform.parent = Parent.transform;
	}

	private void CheckStars()
	{
		int MoveCount = GameManagers.Instance.MoveCount;
		int BestMoveCount = GameManagers.Instance.BestMovBestMoveCount;
		if (MoveCount <= BestMoveCount)
		{
			mPath = "Prefabs/stars/ThreeStars";
			GameManagers.Instance.NumberOfStars = 3;
		}
		else if (MoveCount > BestMoveCount && MoveCount <= BestMoveCount + 9)
		{
			mPath = "Prefabs/stars/TwoStars";
			GameManagers.Instance.NumberOfStars = 2;
		}
		else
		{
			mPath = "Prefabs/stars/OneStars";
			GameManagers.Instance.NumberOfStars = 1;
		}
	}

	private void LoadHome()
	{
		AudioManager.mInstance.play("Back");
		GameManagers.Instance.UpdateUserData();
		GameObject mBoard = GameObject.FindWithTag("MainBoard");
		GameObject mKingBehaviour = GameObject.FindGameObjectWithTag("King");
		mScorePanel.DOAnchorPos(new Vector2(0f, -362f), 1.0f, true).SetEase(Ease.InOutBack).OnComplete(() =>
		{
			GameObject mCanvas = GameObject.FindGameObjectWithTag("Canvas");
			string mPath = "Prefabs/UI Menu Items/MainMenu";
			GameObject mObject = InstantiatePrefab.InstantiateObject(mPath);
			mObject.transform.SetParent(mCanvas.transform, false);
			GridGenerator.Instance.ClearGrid();
			Destroy(mKingBehaviour);
			Destroy(mBoard);
			Destroy(gameObject);
		});
	}

	private void LoadnextLevel()
	{
		mScorePanel.DOAnchorPos(new Vector2(0f, -362f), 1.0f, true).SetEase(Ease.InOutBack).OnComplete(()=>
		{
			Destroy(gameObject);
			GameManagers.Instance.LoadNextLevel();
		});
	}
}
