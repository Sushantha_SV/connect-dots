﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiQuit : MonoBehaviour
{	[SerializeField]
	private Button mYesButton, mNoButton;
	private void Awake()
	{
		
		mYesButton.onClick.AddListener(OnYes);
		mNoButton.onClick.AddListener(OnNo);
	}
	private void OnNo()
	{
		GameObject mCanvas = GameObject.FindGameObjectWithTag("Canvas");
		Destroy(gameObject);
		string mPath = "Prefabs/UI Menu Items/MainMenu";
		GameObject mMenu = InstantiatePrefab.InstantiateObject(mPath);
		mMenu.transform.SetParent(mCanvas.transform, false);
	}

	private void OnYes()
	{
		Application.Quit();
	}

}
