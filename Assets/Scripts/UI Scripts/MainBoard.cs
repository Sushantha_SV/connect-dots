﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class MainBoard : MonoBehaviour
{
	[SerializeField]
	RectTransform mMainBoardPanel;
	[SerializeField]
	private Button mUndoButton, mSettingButton, mBackButton, mReloadbutton;
	[SerializeField]
	private TextMeshProUGUI mBest, mMoveText;
	private string mPath;
	private GameObject mObject, mCanvas, mKingBehaviour,mParentMapper;
	public static MainBoard Instance;
	private void Start()
	{
		GameManagers.Instance.IsPlaying = true;
		ConfigManager.Instance.mMetaData[GameManagers.Instance.Mode].TryGetValue(GameManagers.Instance.Level.ToString(), out LevelData mLevelData);
		if (Instance == null)
			Instance = this;
		else if (Instance != this)
			Destroy(gameObject);
		mCanvas = GameObject.FindWithTag("Canvas");
		mUndoButton.onClick.AddListener(OnUndo);
		mSettingButton.onClick.AddListener(OnSetting);
		mReloadbutton.onClick.AddListener(OnReload);
		mBackButton.onClick.AddListener(OnBack);
		GameManagers.Instance.BestMovBestMoveCount = ((mLevelData.GridRow * mLevelData.GridColumn) - (mLevelData.dots.Count));
		mBest.text = ((mLevelData.GridRow * mLevelData.GridColumn) - (mLevelData.dots.Count)).ToString();
		mParentMapper = GameObject.FindGameObjectWithTag("Mapper");
		mMoveText.text = "00";

		
	}


	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			OnBack();
		}
	}
	void setParent(GameObject inObject)
	{
		inObject.transform.SetParent(mCanvas.transform, false);
	}

	void InstantiateObjecT(string inPath)
	{
		mObject = InstantiatePrefab.InstantiateObject(mPath);
	}
	void SetPath(string inPath)
	{
		mPath=inPath;
	}
	private void OnBack()
	{
		
		AudioManager.mInstance.play("Back");

		DestroyIndicator();
		mKingBehaviour = GameObject.FindGameObjectWithTag("King");
		Destroy(mKingBehaviour);
		GridGenerator.Instance.ClearGrid();
		
		Destroy(gameObject);

		//Scrol View
		SetPath("Prefabs/UI Menu Items/ScrollView");
		InstantiateObjecT(mPath);
		mObject.transform.SetParent(mParentMapper.transform, false);

		SetPath("Prefabs/UI Menu Items/LevelPanel");
		InstantiateObjecT(mPath);
		setParent(mObject);
	}

	internal void OnReload()
	{
		AudioManager.mInstance.play("Refresh");
		DestroyIndicator();
		mKingBehaviour = GameObject.FindGameObjectWithTag("King");
		GameObject.Destroy(mKingBehaviour);
		mMoveText.SetText("000");
		GameManagers.Instance.LoadLevel();
	}

	private void OnUndo()
	{
		AudioManager.mInstance.play("Back");
		DestroyIndicator();
		GameObject mObject; MoveDetails mStackData;
		//First time i.e, only one element in stack
		if (GameManagers.Instance.mStack.Count==1)
		{
			mStackData = GameManagers.Instance.mStack.Pop();
			mObject = mStackData.mTouchedObject;
			ResetLineRenderer(mObject);
		}
		else if(GameManagers.Instance.mStack.Count > 1)
		{
			//remove the top of stack
			mStackData = GameManagers.Instance.mStack.Pop();

			mObject = mStackData.mTouchedObject;
			ResetLineRenderer(mObject);
			//peek top to check both same color
			MoveDetails TopOfStack = GameManagers.Instance.mStack.Peek();
			LineRenderer mLine = TopOfStack.mTouchedObject.GetComponent<LineRenderer>();
			//mLine.positionCount= TopOfStack.mMovedPositions.Length;
			SetFillArray(TopOfStack.mMovedPositions);
			ReDrawLine(mLine,TopOfStack);
			//mLine.SetPositions(TopOfStack.mMovedPositions);
		}
	}
	void ReDrawLine(LineRenderer inLine, MoveDetails StackTop)
	{
		foreach (Vector2 item in StackTop.mMovedPositions)
		{
			GameObject mObject=KingBehaviourControl.Instance.GetTouchedObject(item);
			if(mObject!=null)
			{
				GameManagers.Instance.mConnected[inLine.material.color]=true;
			}
		}
		inLine.positionCount=StackTop.mMovedPositions.Length;
		inLine.SetPositions(StackTop.mMovedPositions);
		
	}
	void ResetLineRenderer(GameObject inObject)
	{

		ResetFillArray(inObject.GetComponent<LineRenderer>());
		if (GameManagers.Instance.mDots.Contains(inObject))
		{
			LineRenderer mLine = inObject.GetComponent<LineRenderer>();
			mLine.positionCount = 0;
		}
		
	}
	void DestroyIndicator()
	{
		//if any Object of Fill Indicator 
		foreach (GameObject mobject in GameManagers.Instance.mFillIndicator)
		{
			Destroy(mobject);
		}
	}
	Vector2 GetRowAndColumn(Vector2 inPosition)
	{
		Vector2 RowCol = new Vector2((inPosition.x - GridGenerator.Instance.mGridOffset.x) / GridGenerator.Instance.mCellSize.x, (inPosition.y - GridGenerator.Instance.mGridOffset.y) / GridGenerator.Instance.mCellSize.y);
		return new Vector2(Mathf.Round(RowCol.y), Mathf.Round(RowCol.x));
	}

	void ResetFillArray(LineRenderer inLineRenderer)
	{
		Vector3[] mPosition = new Vector3[inLineRenderer.positionCount];

		//Get all positions of lineRenderer, in variable  'mPosition'
		inLineRenderer.GetPositions(mPosition);

		foreach (Vector2 position in mPosition)
		{
			Vector2 val = GetRowAndColumn(position);
			if (val != null)
				GameManagers.Instance.FillArray[(int)val.x, (int)val.y] = 0;
		}
		GameManagers.Instance.mConnected[inLineRenderer.material.color] =false;
	}
	void SetFillArray(Vector3 []Positions)
	{
		foreach (Vector2 item in Positions)
		{
			Vector2 RowCol = GetRowAndColumn(item);
			if (RowCol != null)
			{
				GameManagers.Instance.FillArray[(int)RowCol.x, (int)RowCol.y] = 1;
			}
		}
		
	}
	private void OnSetting()
	{
		GameObject.FindGameObjectWithTag("Grid").transform.parent = gameObject.transform;
		AudioManager.mInstance.play("Back");
		mMainBoardPanel.DOAnchorPos(new Vector2(-230f, 0f), 0.2f, true).OnComplete(() =>
		   {
			   GameManagers.Instance.IsPlaying = true;
			   
			   mPath = "Prefabs/UI Menu Items/SettingPanel";
			   mObject = InstantiatePrefab.InstantiateObject(mPath);
			   mObject.transform.SetParent(mCanvas.transform, false);

		   });

		
	}
}
