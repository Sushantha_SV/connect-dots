﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using UnityEngine.EventSystems;


enum SwipeDirection
{
	Up,
	Down,
	Right,
	Left,
	none
}

public class KingBehaviourControl : MonoBehaviour {

	//
	private Vector2 mGridOffSet, mCellSize;
	private GameObject mGrid;
	private string mMode; private int mVertexCount=1;
	//
	[SerializeField]
	//internal int[,] FillArray;
	int mLevel;
	
	private Vector2 mLeftPos, mRightPos, mUpwardPos, mDownPos;
	private Vector2 mPreviousPos, mCurrentPos, mTouchPos, mFingerDownPosition, mFingerUpPosition, mBorder = new Vector2(-3.0f, 3.45f);
	internal Dictionary<GameObject, LineRenderer> gameObjectList = new Dictionary<GameObject, LineRenderer>();
	internal bool SameColorKing =false;
	private GameObject mTouchedKing,mTempObject=null;
	private LineRenderer mLine;
	private Touch touch;
	//---------------------stack Op//------//
	private bool TouchBegan=false, TouchMoved=false;
	//
	GameObject mMoveCountText; int mMoveCount, mSubCount=0;
	private static KingBehaviourControl mInstance;
	public static KingBehaviourControl Instance
	{
		get
		{
			if (mInstance == null)
			{
				mInstance = new KingBehaviourControl();
			}
			return mInstance;
		}
	}
	private void Start()
	{
		mMode = GameManagers.Instance.Mode;
	}

	void Update () {
		if (Input.touchCount > 0 && !EventSystem.current.IsPointerOverGameObject())
		{
			if(GameManagers.Instance.Initialize)
			{
				GameManagers.Instance.Initialize = false;
				Initialize();
			}
			touch = Input.touches[0];
			if (Math.Abs(Camera.main.ScreenToWorldPoint(touch.position).x) < Math.Abs(mBorder.x) && Math.Abs(Camera.main.ScreenToWorldPoint(touch.position).y) < Math.Abs(mBorder.y))
			{
				mTouchPos = GetPosition((int)GetRowAndColumn(Camera.main.ScreenToWorldPoint(touch.position)).x, (int)GetRowAndColumn(Camera.main.ScreenToWorldPoint(touch.position)).y);
				IndicateTouchPhase();
				
			}

		}
	}
	void Initialize()
	{
		mLevel = GameManagers.Instance.Level;
		mCellSize = GridGenerator.Instance.mCellSize;
		mGridOffSet = GridGenerator.Instance.mGridOffset;
		mGrid = GridGenerator.Instance.mGrid;
		GameManagers.Instance.FillArray = new int[GridGenerator.Instance.mRows,GridGenerator.Instance.mColums];
		mMoveCountText = GameObject.FindGameObjectWithTag("MoveCounter");

		switch (mMode)
		{
			case "Easy":
				mVertexCount = 1;
				break;

			case "Normal":
				mVertexCount = 3;
				break;

			case "Hard":
				mVertexCount = 5;
				break;

			case "Master":
				mVertexCount = 7;
				break;

		}
	}
	private void UpdateStack(GameObject inTouched)
	{
		Vector3[] mPositions;
		MoveDetails mMoveDetails;
		if (TouchBegan && TouchMoved)
		{
			TouchBegan = false;
			TouchMoved = false;
			LineRenderer mLine = inTouched.GetComponent<LineRenderer>();
			mPositions = new Vector3[mLine.positionCount];
			mLine.GetPositions(mPositions);
			mMoveDetails = new MoveDetails();
			//set the position and GameObject value
			mMoveDetails.mMovedPositions= mPositions;
			mMoveDetails.mTouchedObject = inTouched;
			//push to stack
			GameManagers.Instance.mStack.Push(mMoveDetails);
		
		}
	}

	private void ContinueLine()
	{
		mLine = mTouchedKing.GetComponent<LineRenderer>();
		//check for swipe at the end of line
		if (mTouchPos == (Vector2)mLine.GetPosition(mLine.positionCount - 1))
			SetPositions(mTouchPos);

		else //check for swipe at the middle of line
		{

			//check for connected king line?
			if (GameManagers.Instance.mConnected[mTouchedKing.GetComponent<SpriteRenderer>().color])
			{
				ReSetConnected(mTouchedKing);
				DeletePart(mTouchedKing, mTouchPos);

				//Reset 4 position from current position
				SetPositions(mTouchPos);
				DrawLine(mTouchPos);

			}
			else
			{
				DeletePart(mTouchedKing, mTouchPos);
				SetPositions(mTouchPos);
				DrawLine(mTouchPos);

			}
		}
	}

	void DeletePart(GameObject inObject, Vector3 inSearchPosition)
	{
		int count = 0;
		mLine = inObject.GetComponent<LineRenderer>();
		int mNumbeerofpoints= mLine.positionCount;


		Vector3[] mPosition = new Vector3[mLine.positionCount];
		mLine.GetPositions(mPosition);
		foreach (Vector3 PositionValue in mPosition)
		{
			if (PositionValue == inSearchPosition)
			{
				//reset the Fill array
				
				//set the fill array based on position filled
				ResetFillArray(mLine);
				mLine.positionCount = count;
				Vector3[] mPositions = new Vector3[mLine.positionCount];
				mLine.GetPositions(mPositions);
				foreach (Vector3 position in mPositions)
				{
					SetFillArray(position);
				}
				break;
			}
			else
			{
				//increase to next count to set renderer count at the end
				count++;
			}
		}
		//to make sure no count
		mSubCount = -1;
	}
	void DeleteOtherLine(GameObject inGameObject)
	{
		if (inGameObject != null)
		{
			ResetFillArray(inGameObject.GetComponent<LineRenderer>());
			ReSetConnected(inGameObject);
			inGameObject.GetComponent<LineRenderer>().positionCount = 0;

		}
	}
	private void DrawLine(Vector2 inPosition)
	{
		if (IsValid(inPosition))
		{
			
			for (int i = 0; i < mVertexCount; i++)
			{
				mLine.positionCount += 1;
				mLine.SetPosition(mLine.positionCount - 1, inPosition);
			}

			mPreviousPos = mCurrentPos;

			//Reset 4 position from current position
			SetPositions(inPosition);
			SetFillArray(inPosition);

			//Add Current Object to Stack 
			//UndoMove.Instance.mStack.Push(mLine);
			mSubCount += 1;
			TouchMoved = true;
			
			if (SameColorKing)
			{
				AudioManager.mInstance.play("Forward");
				SetConnected(mTouchedKing);
				//SameColorKing=false;
			}
		}

	}


	Vector2 GetPosition(int inRow, int inCol)
	{
		Vector2 mCellPosition = new Vector2(inCol * ((mCellSize.x)) + (mGridOffSet.x) + mGrid.transform.position.x, inRow * ((mCellSize.y)) + mGridOffSet.y + mGrid.transform.position.y);
		return mCellPosition;
	}

	Vector2 GetRowAndColumn(Vector2 inPosition)
	{
		Vector2 RowCol = new Vector2((inPosition.x - mGridOffSet.x) / mCellSize.x, (inPosition.y - mGridOffSet.y) / mCellSize.y);
		return new Vector2(Mathf.Round(RowCol.y), Mathf.Round(RowCol.x));
	}

	internal GameObject GetTouchedObject(Vector2 inPosition)
	{
		RaycastHit2D hit = Physics2D.Raycast((inPosition), Vector2.zero);
		if (hit != null && hit.collider != null)
			return (GameObject)hit.transform.gameObject;
		
		else
			return null;

	}

	GameObject GetTouchedByPositionOfLine(Vector2 inSearchPosition)
	{
		mTempObject = null;
		bool searched = false;
		//Search through all line renderers position 
		foreach (KeyValuePair<GameObject, LineRenderer> Line in gameObjectList)
		{
			Vector3[] mPosition = new Vector3[Line.Value.positionCount];
			Line.Value.GetPositions(mPosition);
			foreach (Vector2 position in mPosition)
			{
				if (position == inSearchPosition)
				{
					mTempObject = Line.Key;
					searched = true;
					break;
				}

			}

		}
		if (searched)
			return mTempObject;
		else
			return null;
	}

	
	private void InitialiseLine(GameObject inObject, Vector2 inPosition)
	{
		if (inObject != null)
		{
			mLine = inObject.GetComponent<LineRenderer>();
			mLine.material.color = inObject.GetComponent<SpriteRenderer>().color;
			mLine.positionCount = 1;

			Vector2 RowAndCol = GetRowAndColumn(inPosition);
			mPreviousPos = GetPosition((int)RowAndCol.x, (int)RowAndCol.y);
			mLine.SetPosition(mLine.positionCount - 1, mPreviousPos);

			mFingerUpPosition = touch.position;
			mFingerDownPosition = touch.position;
			

			SetFillArray(inPosition);
			ReSetConnected(inObject);

			mCurrentPos = Vector2.zero;
			
		}
	}
	void InitializeKing()
	{
		InitialiseLine(mTouchedKing, mTouchPos);
		SetPositions(mTouchPos);
		
		if (!gameObjectList.ContainsKey(mTouchedKing))
			gameObjectList.Add(mTouchedKing, mLine);
	}

	private void ResetIndicatorList()
	{
		for (int i = 0; i < GameManagers.Instance.mFillIndicator.Count; i++)
			Destroy(GameManagers.Instance.mFillIndicator[i]);
	}

	private void IndicateTouchPhase()
	{
		if (touch.phase == TouchPhase.Began)
		{
			mSubCount = 0;
			Debug.Log("Began");
			mTouchedKing = GetTouchedObject((mTouchPos));
			ValidateKing(mTouchedKing);
			
			//Check Touched was a King
			if (mTouchedKing != null)
			{
				TouchBegan = true;
				ResetIndicatorList();
				InitializeKing();
			}
			else
			{
				//empty cell having line or no line
				
				mTouchedKing = GetTouchedByPositionOfLine(mTouchPos);
				if (mTouchedKing != null)
				{
					TouchBegan = true;
					ResetIndicatorList();
					ContinueLine();
				}
			}
		}
		else if (touch.phase == TouchPhase.Moved)
		{
			mFingerDownPosition = touch.position;
			mCurrentPos = GetPosition((int)GetRowAndColumn(Camera.main.ScreenToWorldPoint(touch.position)).x, (int)GetRowAndColumn(Camera.main.ScreenToWorldPoint(touch.position)).y);
			Moved();
			Debug.Log("Moved");
		}
		else if (touch.phase == TouchPhase.Ended)
		{
			
			Debug.Log("ended");

			UpdateStack(mTouchedKing);
			UpdateMoveCount();

			mTouchedKing = null;
			if (AllConnected())
			{
				if (CheckFillStatus())
				{
					//All connected and filled

					
					GridGenerator.Instance.ClearGrid();
					Destroy(gameObject);
					LoadScorePanel();
					ResetValues();
					
				}
				else
					FillIndication();
			}
		}
	}

	private void LoadScorePanel()
	{
		string mPath = "Prefabs/UI Menu Items/ScorePanel";
		GameObject mCanvas = GameObject.FindWithTag("Canvas");
		GameObject mMenu = InstantiatePrefab.InstantiateObject(mPath);
		mMenu.transform.SetParent(mCanvas.transform, false);
	}

	private void UpdateMoveCount()
	{
		mMoveCount += mSubCount;
		mMoveCountText.GetComponent<TextMeshProUGUI>().text = mMoveCount.ToString();
		GameManagers.Instance.MoveCount = mMoveCount;
	}

	private void FillIndication()
	{
		string mPath = "Prefabs/Blink/Indicator";
		for(int i=0;i<GridGenerator.Instance.mRows;i++)
		{
			for (int j = 0; j <GridGenerator.Instance.mColums; j++)
			{
				if(GameManagers.Instance.FillArray[i,j]!=1)
				{
					GameObject indicator = InstantiatePrefab.InstantiateObject(mPath) as GameObject;
					indicator.transform.position = GetPosition(i, j);
					indicator.transform.localScale = GridGenerator.Instance.mCellScale;
					GameManagers.Instance.mFillIndicator.Add(indicator);
				}
			}
		}
	}

	private bool CheckFillStatus()
	{
		for(int i=0;i< GridGenerator.Instance.mRows; i++)
			for(int j=0;j< GridGenerator.Instance.mRows; j++)
			{
				if (GameManagers.Instance.FillArray[i, j] == 0)
					return false;
			}
		AudioManager.mInstance.play("LevelComplete");
		return true;
	}

	private bool AllConnected()
	{
		foreach(KeyValuePair<Color,bool> connected in GameManagers.Instance.mConnected)
		{
			if (connected.Value == false)
				return false;

		}
		return true;
	}

	private bool IsValid(Vector2 inPosition)
	{
		//check Next Position is having a King or empty ?
		GameObject TouchedObject = GetTouchedObject(inPosition);
		if (TouchedObject != null)
		{
			//if it is a King ? Check for same color or Different
			if (TouchedObject.GetComponent<SpriteRenderer>().color == mLine.material.color && GameManagers.Instance.mConnected[TouchedObject.GetComponent<SpriteRenderer>().color] == false)
			{
				//Check for next position is same king that is started

				if (GameManagers.Instance.FillArray[(int)GetRowAndColumn(inPosition).x, (int)GetRowAndColumn(inPosition).y] == 1)
				{
					LineRenderer mLine = TouchedObject.GetComponent<LineRenderer>();
					mLine.positionCount = 0;
					return true;
				}
				else
				{
					//set flag and return as valid position
					SameColorKing = true;
					return true;
				}
			}
			//different color king
			else
			{
				//Debug.Log("Different GameObject color");
				return false;
			}
		}
		//check for border 		
		else if (Mathf.Abs(inPosition.x) < Mathf.Abs(mBorder.x) && Mathf.Abs(inPosition.y) < Mathf.Abs(mBorder.y))
		{
			// position is Crosing other line
			GameObject mLineObject = GetTouchedByPositionOfLine(inPosition);
			Debug.Log(GameManagers.Instance.FillArray[(int)GetRowAndColumn(inPosition).x, (int)GetRowAndColumn(inPosition).y]);
			if (GameManagers.Instance.FillArray[(int)GetRowAndColumn(inPosition).x, (int)GetRowAndColumn(inPosition).y] == 1 && mLineObject != null)
			{

				//crossed any other king line
				if (mTouchedKing.tag != mLineObject.tag)
				{
					AudioManager.mInstance.play("CrossedLine");
					//Debug.Log(mLineObject);
					DeleteOtherLine(mLineObject);
					return true;
				}
				//crosed same king line
				else
				{
					DeletePart(mLineObject, inPosition);
					return true;
				}
			}
			else
			return true;
		}
		else
			return false;
	}

	private void Moved()
	{
		Color color = mTouchedKing.GetComponent<SpriteRenderer>().color;
		switch (DetectSwipe())
		{
			case SwipeDirection.Up:
				if (mCurrentPos.Equals(mUpwardPos) && !GameManagers.Instance.mConnected[color])
				{
					//Up
					DrawLine(mUpwardPos);
				}
				break;
			case SwipeDirection.Down:
				if (mCurrentPos.Equals(mDownPos) && !GameManagers.Instance.mConnected[color])
				{
					//Down
					DrawLine(mDownPos);
				}
				break;
			case SwipeDirection.Left:
				if (mCurrentPos.Equals(mLeftPos) && !GameManagers.Instance.mConnected[color])
				{
					//Left
					DrawLine(mLeftPos);
				}
				break;
			case SwipeDirection.Right:
				if (mCurrentPos.Equals(mRightPos) && !GameManagers.Instance.mConnected[color])
				{
					//Right
					DrawLine(mRightPos);
				}
				break;
			default:
				mCurrentPos = Vector2.zero;
				break;
		}
	}

	private SwipeDirection DetectSwipe()
	{
		if (IsVerticalSwipe())
		{
			var direction = mFingerDownPosition.y - mFingerUpPosition.y > 0 ? SwipeDirection.Up : SwipeDirection.Down;
			mFingerUpPosition = mFingerDownPosition;
			return direction;
		}
		else
		{
			var direction = mFingerDownPosition.x - mFingerUpPosition.x > 0 ? SwipeDirection.Right : SwipeDirection.Left;
			mFingerUpPosition = mFingerDownPosition;
			return direction;
		}
	}
	private bool IsVerticalSwipe()
	{
		return VerticalMovementDistance() > HorizontalMovementDistance();
	}
	private float VerticalMovementDistance()
	{
		return Mathf.Abs(mFingerDownPosition.y - mFingerUpPosition.y);
	}

	private float HorizontalMovementDistance()
	{
		return Mathf.Abs(mFingerDownPosition.x - mFingerUpPosition.x);

	}
	void ReSetConnected(GameObject inTouchedObject)
	{
		//set the color of Dictionary to false., i.e, Not Connected
		GameManagers.Instance.mConnected[inTouchedObject.GetComponent<SpriteRenderer>().color] = false;
		//flag to ensure Not to execute part i.e., in DrawLine()
		SameColorKing = false;
	}

	internal void ResetFillArray(LineRenderer inLineRenderer)
	{
		Vector3[] mPosition = new Vector3[inLineRenderer.positionCount];

		//Get all positions of lineRenderer, in variable  'mPosition'
		inLineRenderer.GetPositions(mPosition);

		foreach (Vector2 position in mPosition)
		{
			Vector2 val = GetRowAndColumn(position);
			if (val != null)
				GameManagers.Instance.FillArray[(int)val.x, (int)val.y] = 0;

		}

	}

	internal void ResetValues()
	{
		ResetIndicatorList();
		Array.Clear(GameManagers.Instance.FillArray, 0, GameManagers.Instance.FillArray.Length);
		gameObjectList.Clear();
		GameManagers.Instance.mConnected.Clear();
		mMoveCountText.GetComponent<TextMeshProUGUI>().SetText("000");
		mMoveCount = 0;
		mSubCount = 0;
		/*for (int i = 0; i < GridGenerator.Instance.mRows; i++)
			for(int j=0;j< GridGenerator.Instance.mColums; j++)
			FillArray[i, j] = 0;*/

	}
	void SetConnected(GameObject inTouchedObject)
	{
		GameManagers.Instance.mConnected[inTouchedObject.GetComponent<SpriteRenderer>().color] = true;
		SameColorKing = false;
	}

	void SetFillArray(Vector2 inPosition)
	{
		Vector2 RowCol = GetRowAndColumn(inPosition);
		if (RowCol != null)
		{
			GameManagers.Instance.FillArray[(int)RowCol.x, (int)RowCol.y] = 1;
		}

	}
	void SetPositions(Vector2 inPosition)
	{
		Vector2 RowAndColumn = GetRowAndColumn(inPosition);
		mLeftPos = GetPosition((int)RowAndColumn.x, ((int)RowAndColumn.y - 1));
		mRightPos = GetPosition((int)RowAndColumn.x, ((int)RowAndColumn.y + 1));
		mUpwardPos = GetPosition(((int)RowAndColumn.x + 1), ((int)RowAndColumn.y));
		mDownPos = GetPosition(((int)RowAndColumn.x - 1), ((int)RowAndColumn.y));
	}
	private void ValidateKing(GameObject inTouchedKing)
	{
		if (inTouchedKing != null)
		{
			//validate to remove only one line of touched king of same color is present
			foreach (GameObject item in GameObject.FindGameObjectsWithTag(inTouchedKing.tag))
			{
				LineRenderer mLineRenderer = item.GetComponent<LineRenderer>();
				if (mLineRenderer.positionCount > 1)
				{

					ResetFillArray(mLineRenderer);
					Debug.Log("Validate");
					mLineRenderer.positionCount = 0;
					break;
				}

			}
		}
	}
}
