﻿using UnityEngine;

public class InstantiatePrefab : MonoBehaviour
{
	private static GameObject mObject;
	public static GameObject InstantiateObject(string inPath)
	{
		mObject = Resources.Load<GameObject>(inPath);
		return Instantiate(mObject) as GameObject;
	}
}