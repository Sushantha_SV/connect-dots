﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
public class Dots
{
    public List<int> row { get; set; }
    public List<int> col { get; set; }
    public string color { get; set; }
}
public class Level
{
	public int level { get; set; }
	public int NumberOfStars { get; set; }
	public bool Unlocked { get; set; }
}
public class UserStatus
{
	public Dictionary<int,Level> mLevelStatus { get; set; }
}
public class SettingPref
{
	public bool SoundOn;
	public bool VibrationOn;
}
public class LevelData
{
    public List<Dots> dots;
    public int GridRow{get;set;}
    public int GridColumn{get;set;}
}

public class ConfigManager
{
	private string mDataPath;
	public Dictionary<string, Dictionary<string,LevelData>> mMetaData;
	public Dictionary<string,UserStatus> mUserDetails;

	private static ConfigManager mInstance;
	private ConfigManager() { }
	public static ConfigManager Instance
	{
		get
		{
			if (mInstance == null)
			{
				mInstance = new ConfigManager();
			}
			return mInstance;
		}
	}
	public void ConfigGame()
	{
		mUserDetails = LoadUserDetails();
		mMetaData = LoadMetaData();
		LoadSettingPref();
	}

	public SettingPref LoadSettingPref()
	{
		mDataPath = Path.Combine(Application.persistentDataPath, "SettingPref.txt");
		if (File.Exists(mDataPath))
		{
			using (StreamReader streamReader = File.OpenText(mDataPath))
			{
				string jsonString = streamReader.ReadToEnd();
				SettingPref data = JsonConvert.DeserializeObject<SettingPref>(jsonString);

				GameManagers.Instance.IsSoundOn = data.SoundOn;
				GameManagers.Instance.IsVibrationOn = data.VibrationOn;
				return data;
			}
		}
		else
		{
			SettingPref mdata = new SettingPref()
			{
				SoundOn = true,
				VibrationOn = true
			};
			string jsonString = JsonConvert.SerializeObject(mdata, Formatting.Indented);

			using (StreamWriter streamWriter = File.CreateText(mDataPath))
			{
				streamWriter.Write(jsonString);
			}
			using (StreamReader streamReader = File.OpenText(mDataPath))
			{
				jsonString = streamReader.ReadToEnd();
				mdata = JsonConvert.DeserializeObject<SettingPref>(jsonString);

				return mdata;
			}
		}	
	}

	Dictionary<string, Dictionary<string, LevelData>> LoadMetaData()
	{
		mDataPath = "MetaData/LevelData";
		TextAsset targetFile = Resources.Load<TextAsset>(mDataPath);
		string jsonString = targetFile.text;
		Dictionary<string, Dictionary<string,LevelData>> data = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, LevelData>>>(jsonString);
		return data;
	}
	public Dictionary<string, UserStatus> LoadUserDetails()
	{
		mDataPath = Path.Combine(Application.persistentDataPath, "UserDetails.txt");
		//File.Delete(mDataPath);
		if (File.Exists(mDataPath))
		{
			Dictionary<string, UserStatus> data;
			using (StreamReader streamReader = File.OpenText(mDataPath))
			{

				string jsonString = streamReader.ReadToEnd();
				data = JsonConvert.DeserializeObject<Dictionary<string, UserStatus>>(jsonString);
				return data;
			}
		}
		else
		{
			Level Level = new Level
			{
				level = 1,
				NumberOfStars = 0,
				Unlocked = true
			};

			UserStatus mLevelStatus = new UserStatus
			{
				mLevelStatus = new Dictionary<int, Level>(),
			};

			mLevelStatus.mLevelStatus.Add(1,Level);
			Dictionary<string, UserStatus> UserStatus = new Dictionary<string, UserStatus>
			{
				{ "Easy", mLevelStatus },
				{ "Normal", mLevelStatus },
				{ "Hard", mLevelStatus },
				{ "Master", mLevelStatus }
			};
			string jsonString = JsonConvert.SerializeObject(UserStatus, Formatting.Indented);
			
			using (StreamWriter streamWriter = File.CreateText(mDataPath))
			{
				streamWriter.Write(jsonString);
			}
			using (StreamReader streamReader = File.OpenText(mDataPath))
			{
				jsonString = streamReader.ReadToEnd();
				UserStatus = JsonConvert.DeserializeObject<Dictionary<string,  UserStatus>>(jsonString);
				return UserStatus;
			}
		}
	}
	public void StoreUserDetails()
	{
		mDataPath = Path.Combine(Application.persistentDataPath, "UserDetails.txt");
		mUserDetails = GameManagers.Instance.mLevelStatus;
		string jsonString = JsonConvert.SerializeObject(mUserDetails, Formatting.Indented);
		using (StreamWriter streamWriter = File.CreateText(mDataPath))
		{
			streamWriter.Write(jsonString);
		}
	}
	internal void StoreSettingPreference()
	{

		mDataPath = Path.Combine(Application.persistentDataPath, "SettingPref.txt");
		SettingPref mRef = new SettingPref();

		mRef.SoundOn = GameManagers.Instance.IsSoundOn;

		mRef.VibrationOn = GameManagers.Instance.IsVibrationOn;

		string jsonString = JsonConvert.SerializeObject(mRef, Formatting.Indented);
		using (StreamWriter Writer = File.CreateText(mDataPath))
		{
			Console.WriteLine(mRef.VibrationOn);
			Writer.Write(jsonString);
		}
	}
}
