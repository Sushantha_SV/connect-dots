﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GridGenerator {
    
    //---------------------------------------------//
    internal int mRows;
    internal int mColums;
	[SerializeField]
    internal Vector2 mGridSize;
    private string mPath;
    //---------------------------------------------//
    private string mKingTag;
    public Vector2 mGridOffset;
    public Vector2 mCellSize;
    internal Vector2 mCellScale;
    internal Vector2 mNewCellSize;
    public GameObject mGrid;
    private GameObject mParent;
	private List<Dots> mDots;
	private LevelData mLevelData;
	private KingBehaviourControl mKings;

	//---------------------------------------------//

	private static GridGenerator mInstance;
    private GridGenerator() {}
    public static GridGenerator Instance
    {
       get{
         if(mInstance == null){
            mInstance = new GridGenerator();
         }
         return mInstance;
       }
    }
    void InitialiseGridParams(int mLevel)
    {
		mRows=mLevelData.GridRow;
		mColums=mLevelData.GridColumn;
        mGrid = GameObject.FindWithTag("Grid");
        mGridSize = new Vector2(4.88f,5.63f);    
        mNewCellSize = new Vector2(mGridSize.x / (float)mColums, mGridSize.y / (float)mRows);
        CreateGridLines();
    }
    public void InitialiseKings(int inLevel, string inMode)
    {
		ConfigManager.Instance.mMetaData[inMode].TryGetValue(inLevel.ToString(), out mLevelData);
		mDots = mLevelData.dots;

		Color color;
		Debug.Log(inLevel);
        InitialiseGridParams( inLevel);															
        for(int i=0;i<mDots.Count;i++)
        {

            color= SetColor(mDots[i].color);
			for(int j=0;j<2;j++)
            PlaceKings(mDots[i].row[j],mDots[i].col[j],color);
			//PlaceKings(0,1,color);
		}

    }
    private Color SetColor(string inColor)
    {
        Color color;
         switch(inColor)
            {
                case "red": 
                    color= Color.red;
                    mKingTag="red";
                    break;
                case "blue":
                    color=Color.blue;
                    mKingTag="blue";
                    break;
                case "green":
                    color= Color.green;
                    mKingTag="green";
                    break;
                case "yellow":
                    color= Color.yellow;
                    mKingTag="yellow";
                    break;
                case "white":
                    color= Color.white;
                    mKingTag="white";
                    break;
			case "cyan":
				color = Color.cyan;
				mKingTag = "cyan";
				break;
			case "megenta":
				color = Color.magenta;
				mKingTag = "megenta";
				break;
			default: color=Color.black;
                break;
            }
        return color;    
    }
    private void PlaceKings(int inRow,int inCol,Color color)
    {
		LineRenderer mLine;
        
        mPath = "Prefabs/King/baseball";        
        GameObject mObject = InstantiatePrefab.InstantiateObject(mPath) as GameObject;
        mObject.name= "King "+mKingTag;
        mObject.tag= mKingTag;

		if(!GameManagers.Instance.mConnected.ContainsKey(color))
        GameManagers.Instance.mConnected.Add(color,false);
        //Debug.Log(GameManagers.Instance.mDots.IndexOf(mObject));
       
       

        mCellSize = mObject.GetComponent<SpriteRenderer>().bounds.size;
		// mCellScale.x = mNewCellSize.x / mCellSize.x;
		// mCellScale.y = mNewCellSize.y / mCellSize.y;
		// mCellSize = mNewCellSize; 
		// mGridOffset.x = -(mGridSize.x / 2) + mCellSize.x / 2;
		// mGridOffset.y = -(mGridSize.y / 2) + mCellSize.y / 2;
		// Vector2 mCellPosition = new Vector2(inCol * ((mCellSize.x)) + (mGridOffset.x) + mGrid.transform.position.x, inRow * ((mCellSize.y)) + mGridOffset.y + mGrid.transform.position.y);

		//Debug.Log(mCellPosition);

		// Vector2 mCellPosition =GetPosition(inRow,inCol);

		mObject.GetComponent<Rigidbody2D>().DOJump((Vector2)GetPosition(inRow, inCol),1.2f,1,2,false);
        //mObject.transform.position = GetPosition(inRow,inCol);
        mObject.transform.localScale = new Vector2(mCellScale.x-0.02f, mCellScale.y-0.02f);

        mObject.GetComponent<SpriteRenderer>().color=color;
        mObject.GetComponent<SpriteRenderer>().sortingLayerName= "Foreground";
        mObject.transform.parent = mGrid.transform;

		mLine = mObject.GetComponent<LineRenderer>();
		mLine.SetWidth((1.5f / mRows), (1.5f / mRows));
		AddToList(mObject);
		

	}
	void AddToList(GameObject inObject)
	{
		GameManagers.Instance.mDots.Add(inObject);
	}
    internal Vector2 GetPosition(int row,int col)
    {
        mCellScale.x = mNewCellSize.x / mCellSize.x;
        mCellScale.y = mNewCellSize.y / mCellSize.y;
        mCellSize = mNewCellSize; 
        mGridOffset.x = -(mGridSize.x / 2) + mCellSize.x / 2;
        mGridOffset.y = -(mGridSize.y / 2) + mCellSize.y / 2;
        Vector2 mCellPosition = new Vector2(col * ((mCellSize.x)) + (mGridOffset.x) + mGrid.transform.position.x, row * ((mCellSize.y)) + mGridOffset.y + mGrid.transform.position.y);
        return mCellPosition;
    }
    void CreateGridLines()
    {
        mPath = "Prefabs/king/GridLine";
        Vector2 newGridScale= new Vector2(mGridSize.x/(float)(mColums),mGridSize.y/(float)(mRows));
        Vector2 mGridOffset ;
		mParent = GameObject.Find("Grid");

		mGridOffset.x= -(mGridSize.x/2);
        mGridOffset.y=-(mGridSize.y/2);
       
        
        for(int i=1;i<=mRows-1;i++)
        {
            Vector2 mHorizontalPosition= new Vector2((newGridScale.x+mGrid.transform.position.x+mGridOffset.x),(i*newGridScale.y+mGrid.transform.position.y+mGridOffset.y));
			GameObject mObject = InstantiatePrefab.InstantiateObject(mPath) as GameObject;
			mObject.transform.parent = mParent.transform;
			mObject.transform.position = new Vector2(0f, mHorizontalPosition.y);

			AddToList(mObject);

			Vector2 mVerticalPosition= new Vector2((i*newGridScale.x+mGrid.transform.position.x+mGridOffset.x),(newGridScale.y+mGrid.transform.position.y+mGridOffset.y));
			mObject = InstantiatePrefab.InstantiateObject(mPath) as GameObject;
			mObject.transform.position= new Vector2(mVerticalPosition.x,0f);
			mObject.transform.Rotate(new Vector3(0, 0, 90));
			mObject.transform.parent = mParent.transform;

			
			AddToList(mObject);
		}

    }
	public void ClearGrid()
	{
		for (int index = 0; index < GameManagers.Instance.mDots.Count; index++)
		{

			UnityEngine.Object.Destroy(GameManagers.Instance.mDots[index].gameObject);
			
		}
	}
	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireCube(Vector2.zero, mGridSize);
	}

}
