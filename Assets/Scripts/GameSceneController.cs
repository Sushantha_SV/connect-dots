﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class GameSceneController : MonoBehaviour
{
    public static GameSceneController mInstance;
	private GameObject mMenu, mCanvas;
	private string mPath;
  
    void Awake()
    {
		mCanvas = GameObject.FindGameObjectWithTag("Canvas");
		if (mInstance == null)
            mInstance = this;
        else if(mInstance != this)
            Destroy(gameObject); 
      
    }
    // Start is called before the first frame update
    void Start()
    {
		ConfigManager.Instance.ConfigGame();
		mPath = "Prefabs/UI Menu Items/MainMenu";
		mMenu = InstantiatePrefab.InstantiateObject(mPath);
		mMenu.transform.SetParent(mCanvas.transform, false);
		//GridGenerator.Instance.InitialiseKings(GameManagers.Instance.Level);
		//mPath = "Prefabs/UI/BackGround";
		//mMenu = InstantiateObjects.InstantiatePrefab(mPath);
		//MainMenuUI.Instance.Load(mMenu);
	}



   
}
