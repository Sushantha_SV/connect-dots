﻿using UnityEngine.Audio;
using UnityEngine;
using System;

[System.Serializable]
public class Sound
{
	public string name;
	public AudioClip clip;
	[Range(0.1f, 3f)]
	public float pitch;
	public AudioSource mSource;

}
public class AudioManager : MonoBehaviour
{

	[Range(0f, 1f)]
	public float volume;
	public static AudioManager mInstance;
	public Sound[] mSound;
	private bool mIsSoundOn=true, mVibration=true;
	internal bool IsSoundOn
	{
		get
		{
			return mIsSoundOn;
		}
		set
		{
			mIsSoundOn = value;
		}
	}
	internal bool IsVibrationOn
	{
		get
		{
			return mVibration;
		}
		set
		{
			mVibration = value;
		}
	}
	void Awake()
	{
		if(mInstance == null)
			mInstance = this;
		else if(mInstance != this)
			Destroy(gameObject);  
		foreach(Sound sound in mSound)
		{
			sound.mSource = gameObject.AddComponent<AudioSource>();
			sound.mSource.clip = sound.clip;
		}
	}

	
	public void play(string inName)
	{
		Sound SoundClip=Array.Find(mSound, sound => sound.name == inName);
		SoundClip.mSource.volume = volume;

		if (SoundClip != null)
			SoundClip.mSource.Play();
	}

}
