﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using UnityEngine;

public class TestCamera : MonoBehaviour
{
    private const float mReferenceAspectRatio =(9f/16f);
    private const float mReferenceHeight = 10f;
    private float mHeight, mWidth, mAspectRatio;
    public Vector2 BoardSize
    {
        get
        {
            return mBoardSize;
        }
    }
    [SerializeField]
    private BoxCollider2D mBackgroundCollider;
    [SerializeField]
    private BoxCollider2D mBoardCollider;
    private Vector2 mBoardSize;
    public static TestCamera mInstance;
  
    void Awake()
    {
        if(mInstance == null)
            mInstance = this;
        else if(mInstance != this)
            Destroy(gameObject); 
        Camera cam = Camera.main;
        mHeight =cam.orthographicSize*2;
        mAspectRatio =cam.aspect;
        mWidth=mAspectRatio*mHeight;
    }
    


    public void AdjustScale()
    {
        AdjustBackground();
        AdjustBoard();
    }
    public void AdjustBackground()
    {

        float offsetX, offsetY;
        Vector2 size;
        size= mBackgroundCollider.size;

        offsetX = mReferenceAspectRatio*mReferenceHeight - size.x;
        offsetY = mReferenceHeight - size.y;

        float newSizeX = mWidth - offsetX;
        float newSizeY = mHeight -offsetY;

        float xScale = newSizeX/size.x;
        float yScale = newSizeY/size.y;

        mBackgroundCollider.gameObject.transform.localScale = new Vector2(xScale,yScale);

    }
    public void AdjustBoard()
    {
        float offsetX, offsetY;
        Vector2 size;
        size = mBoardCollider.size;
        offsetX = mReferenceAspectRatio * mReferenceHeight - size.x;
        offsetY = mReferenceHeight - size.y;

        float newSizeX = mWidth - offsetX;
        float newSizeY = mHeight -offsetY;
       
        float xScale = newSizeX/size.x;
        float yScale = newSizeY/size.y;
        if(xScale < yScale)
        {
            yScale = xScale;
            mBoardSize = new Vector2(newSizeX,newSizeX);
        }      
        else
        {
            xScale = yScale;
            mBoardSize = new Vector2(newSizeY,newSizeY);
        }
            
        mBoardCollider.gameObject.transform.localScale = new Vector2(xScale,yScale);
    }
}
